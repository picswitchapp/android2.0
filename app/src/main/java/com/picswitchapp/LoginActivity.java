package com.picswitchapp;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseFacebookUtils;
import com.parse.ParseFile;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

/**
 * A login screen that offers login via Facebook.
 */
public class LoginActivity extends AppCompatActivity {

    Button mFacebookLoginButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mFacebookLoginButton = (Button) findViewById(R.id.login_with_facebook_button);
        mFacebookLoginButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                mFacebookLoginButton.setText("Connecting to Facebook...");
                attemptLogin();
            }
        });
    }

    private void attemptLogin() {

        ParseFacebookUtils.logInWithReadPermissionsInBackground(LoginActivity.this, null, new LogInCallback() {
            @Override
            public void done(ParseUser user, ParseException err) {
                if (err == null) {
                    System.out.println("attemptLogin callback done");
                    if (user == null) {
                        Log.d("MyApp", "Uh oh. The user cancelled the Facebook login.");
                        Toast.makeText(LoginActivity.this, "Facebook login failed.", Toast.LENGTH_LONG).show();
                    } else if (user.isNew()) {
                        Log.d("MyApp", "User signed up and logged in through Facebook!");
                        Toast.makeText(LoginActivity.this, "Facebook sign up successful.", Toast.LENGTH_LONG).show();
                        getUserDetailsFromFB();
                    } else {
                        Log.d("MyApp", "User logged in through Facebook!");
                        Toast.makeText(LoginActivity.this, "Facebook login successful.", Toast.LENGTH_LONG).show();
                        LoginActivity.this.finish();
                    }
                } else {
                    err.printStackTrace();
                }
            }
        });
    }

    //String email;
    String name;
    String fbID;
    Bitmap mProfileImage;

    private void getUserDetailsFromFB(){
        Bundle params = new Bundle();
        params.putString("fields", "id,name,link,gender,birthday,email,picture");

        new GraphRequest(
                AccessToken.getCurrentAccessToken(),
                "/me",
                params,
                HttpMethod.GET,
                new GraphRequest.Callback() {
                    public void onCompleted(GraphResponse response) {
                        try {
                            //email = response.getJSONObject().getString("email");
                            System.out.println("response from fb: " + response.toString());
                            name = response.getJSONObject().getString("name");
                            fbID = response.getJSONObject().getString("id");
                            JSONObject picture = response.getJSONObject().getJSONObject("picture");
                            JSONObject data = picture.getJSONObject("data");
                            //  Returns 50x50 profile picture
                            String pictureUrl = data.getString("url");
                            System.out.println("pictureUrl for profile image: " + pictureUrl);
                            new ProfilePhotoAsync(pictureUrl).execute();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
        ).executeAsync();
    }

    private class ProfilePhotoAsync extends AsyncTask<String, String, String> {
        public Bitmap bitmap = null;
        String url;

        public ProfilePhotoAsync(String url) {
            this.url = url;
        }

        @Override
        protected String doInBackground(String... params) {
            // Fetching data from URI and storing in bitmap
            try {
                URL aURL = new URL(url);
                URLConnection conn = aURL.openConnection();
                conn.connect();
                InputStream is = conn.getInputStream();
                BufferedInputStream bis = new BufferedInputStream(is);
                bitmap = BitmapFactory.decodeStream(bis);
                bis.close();
                is.close();
            } catch (IOException e) {
                Log.e("IMAGE", "Error getting bitmap", e);
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            mProfileImage = bitmap;
            setNewUserFields();
        }
    }

    private void setNewUserFields(){
        final ParseUser mUser = ParseUser.getCurrentUser();
        mUser.put("name", name);
        //mUser.setEmail(email);

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        mProfileImage.compress(Bitmap.CompressFormat.JPEG, 70, stream);
        byte[] data = stream.toByteArray();
        String thumbName = mUser.getUsername().replaceAll("\\s+", "");
        final ParseFile parseFile = new ParseFile(thumbName + "_thumb.jpg", data);

        parseFile.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                mUser.put("profilePicture", parseFile);
                mUser.put("facebookId", fbID);
                mUser.saveInBackground(new SaveCallback() {
                    @Override
                    public void done(ParseException e) {
                        Toast.makeText(LoginActivity.this, "New user:" + name + " Signed up",
                                Toast.LENGTH_SHORT).show();
                        LoginActivity.this.finish();
                    }
                });
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode, resultCode, data);
        ParseFacebookUtils.onActivityResult(requestCode, resultCode, data);
    }
}

