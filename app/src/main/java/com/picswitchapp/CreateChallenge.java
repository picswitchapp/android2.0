package com.picswitchapp;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.parse.FunctionCallback;
import com.parse.ParseCloud;
import com.parse.ParseUser;
import com.parse.ParseException;

import java.util.HashMap;

public class CreateChallenge extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_challenge);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        Button testChallenge = (Button) findViewById(R.id.test_challenge_creation);
        testChallenge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view){
                HashMap<String, Object> params = new HashMap<String, Object>();
                String id = ParseUser.getCurrentUser().getObjectId();
                params.put("memberIds", id);
                params.put("timeLimit", 24);
                params.put("category", "Test");
                ParseCloud.callFunctionInBackground("createRandomChallenge", params, new FunctionCallback<Object>(){
                    @Override
                    public void done(Object object, ParseException e) {
                        if(e == null){
                            Toast.makeText(CreateChallenge.this, "no errors", Toast.LENGTH_LONG).show();
                        }else{
                            Toast.makeText(CreateChallenge.this, "there were errors", Toast.LENGTH_LONG).show();
                            e.printStackTrace();
                        }
                    }
                });
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
